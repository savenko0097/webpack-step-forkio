const path = require("path");

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


const mode = process.env.NODE_ENV || 'development';
const devMode = mode === 'development';

const target = devMode ? 'web' : 'browserslist';
const devtool = devMode ? 'source-map' : undefined;

module.exports = {
    mode,
    target,
    devtool,
    devServer:{
        port: 3000,
        open: true,
        hot: true,
    },
    entry: ['@babel/polyfill',path.resolve(__dirname, 'src', 'index.js')],
    output: {
        path: path.resolve(__dirname, 'dist'),
        clean: true,
        filename: '[name].js', // name of file js in dist
        assetModuleFilename: 'assets/[name][ext]',
    },
    plugins: [

      
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'src', 'index.html'),
        }),


        new HtmlWebpackPlugin({
          template: './src/html/_header.html',
          filename: '_header.html',
          inject: false
        }),
        new HtmlWebpackPlugin({
          template: './src/html/_editor.html',
          filename: '_editor.html',
          inject: false
        }),

        new HtmlWebpackPlugin({
          template: './src/html/_features.html',
          filename: '_features.html',
          inject: false
        }),

        new HtmlWebpackPlugin({
          template: './src/html/_about-fork.html',
          filename: '_about-fork.html',
          inject: false
        }),

        new HtmlWebpackPlugin({
          template: './src/html/_pricing.html',
          filename: '_pricing.html',
          inject: false
        }),
        
    new MiniCssExtractPlugin({
            filename: '[name].css' // or filename: '[name].[contenthash].css
        })
    
    ],

        

        module: {
            rules: [
              {
                test: /\.html$/i,
                 use: [
                  {
                    loader: 'html-loader',
                    options: {
                      minimize: true,
                    },
                  },
                  {
                    loader: 'html-include-loader',
                  },
                ],
              },


              // ======STYLES====== //
              {
                test: /\.s[ac]ss$/i,
                use: [
                // Creates `style` nodes from JS strings
                devMode ? "style-loader" : MiniCssExtractPlugin.loader,
                // Translates CSS into CommonJS
                "css-loader",

                {
                    loader: 'postcss-loader',
                    options: {
                        postcssOptions:{
                            plugins: [require('postcss-preset-env')],
                        }
                    }
                },


                // Compiles Sass to CSS
                "sass-loader"
                ]
              },


            {
                    test: /\.(jpe?g|png|webp|gif|svg)$/i,
                    use:{
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg: {
                              progressive: true,
                            },
                            // optipng.enabled: false will disable optipng
                            optipng: {
                              enabled: false,
                            },
                            pngquant: {
                              quality: [0.65, 0.90],
                              speed: 4
                            },
                            gifsicle: {
                              interlaced: false,
                            },
                            // the webp option will enable WEBP
                            webp: {
                              quality: 75
                            }
                          }
                    },
                    type: 'asset/resource',
                    generator:{
                        filename: 'img/[name][ext]'
                    }
                },

              {
                    test: /\.(?:js|mjs|cjs)$/i,
                    exclude: /node_modules/,
                    use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                        ['@babel/preset-env', { targets: "defaults" }]
                        ]
                    }
                    }
               },
               

            ],

        }
}